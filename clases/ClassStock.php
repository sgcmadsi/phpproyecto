<?php
require_once('db_abstract_class.php');

class Stock extends db_abstract_class{
	
	private $IdStock;
    private $Cantidad;
    private $ValorUnitario;
    private $ValorVenta;
    private $IdPedido;
    private $IdSuministro;
	
	


	/* Setters and Getters*/
  public function getIdStock(){
        return $this->IdStock;
    }
    
    private function setIdStock($IdStock){
        $this->IdStock = $IdStock;
        return $this;
    }

  

      public function getCantidad(){
        return $this->Cantidad;
    }
    
    private function setCantidad($Cantidad){
        $this->Cantidad = $Cantidad;
        return $this;
    }

    public function getValorUnitario(){
        return $this->ValorUnitario;
    }
    
    private function setValorUnitario($ValorUnitario){
        $this->ValorUnitario = $ValorUnitario;
        return $this;
    }


    public function getValorVenta(){
        return $this->ValorVenta;
    }
    
    private function setValorVenta($ValorVenta){
        $this->ValorVenta = $ValorVenta;
        return $this;
    }

      public function getIdPedido(){
        return $this->IdPedido;
    }
    
    private function setIdPedido($IdPedido){
        $this->IdPedido = $IdPedido;
        return $this;
    }

      public function getIdSuministro(){
        return $this->IdSuministro;
    }
    
    private function setIdSuministro($IdSuministro){
        $this->IdSuministro = $IdSuministro;
        return $this;
    }

  

   

    function __destruct() {
        $this->Disconnect();
    }

	public function __construct($user_data=array()){
        parent::__construct();
		if(count($user_data)>1){
			foreach ($user_data as $campo=>$valor){
                $this->$campo = $valor;
			}
		}else {
			$this->IdStock = "";
			$this->Cantidad = "";
			$this->ValorUnitario = "";
			$this->ValorVenta = "";
			$this->IdPedido = "";
            $this->IdSuministro = "";

           

			
			
		}
    }

    public function insertar(){
        $arrUser = (array) $this;
        $this->insertRow("INSERT INTO Stock
            VALUES ('?', ?, ?, ?, ?,?)", array( 
            
            $this->Cantidad,
            $this->ValorUnitario,
            $this->ValorVenta,
            $this->IdPedido,
            $this->IdSuministro,
          
            )
        );
		$this->Disconnect();
    }

    public function editar(){
		
        return $this->user_login;
    }

    public function eliminar(){
        return $this->user_login;
    }

    public static function buscarForId($id){
		if ($id > 0){
			$st = new Stock();
			$getrow = $st->getRow("SELECT * FROM Stock WHERE IdStock =?", array($id));
			$st->id = $getrow['IdStock'];
			$st->Nombre= $getrow['Cantidad'];
			$st->Descripcion = $getrow['ValorUnitario'];
			$st->Tipo= $getrow['ValorVenta'];
            $pe= new Pedido();
            $pe->buscarForId($id)->getIdPedido("IdPedido");
            $st->setIdPedido($pe);

            $s= new Suministro();
            $s->buscarForId($id)->getIdSuministro("IdSuministro");
            $st->setIdSuministro($s);
         
         			
			$st->Disconnect();
			return $st;
		}else{
			return NULL;
		}

    }
    public static function getAll(){
	
	    return Stock::buscar("SELECT * FROM Stock");
    }
    
    public static function buscar($query){
        $arraystock = array();
        $tmp = new Stock();
        $getrows = $tmp->getrows($query);
        
        foreach ($getrows as $valor) {
            $stock = new Stock();
            $stock->IdStock = $valor['IdStock'];
            $stock->Cantidad = $valor['Cantidad'];
            $stock->ValorUnitario = $valor['ValorUnitario'];
            $stock->ValorVenta =  $valor['ValorVenta'];
            $stock->IdPedido = $valor['IdPedido'];
            $stock->IdSuministro = $valor['IdSuministro'];
            array_push($arraystock, $stock);
        }
        $tmp->Disconnect();
        return $arraystock;
    }

}
?>