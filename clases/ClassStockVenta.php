<?php
require_once('db_abstract_class.php');

class StockVenta extends db_abstract_class{
	
    private $IdStockVenta;
	private $IdStock;
    private $IdVenta;

	


	/* Setters and Getters*/
    public function getIdStockVenta(){
        return $this->IdStockVenta;
    }
    
    private function setIdStockVenta($IdStockVenta){
        $this->IdStockVenta = $IdStockVenta;
        return $this;
    }


    public function getIdStock(){
        return $this->IdStock;
    }
    
    private function setIdStock($IdStock){
        $this->IdStock = $IdStock;
        return $this;
    }

  

      public function getIdVenta(){
        return $this->IdVenta;
    }
    
    private function setIdVenta($IdVenta){
        $this->IdVenta = $IdVenta;
        return $this;
    }

  

   

    function __destruct() {
        $this->Disconnect();
    }

	public function __construct($user_data=array()){
        parent::__construct();
		if(count($user_data)>1){
			foreach ($user_data as $campo=>$valor){
                $this->$campo = $valor;
			}
		}else {
           
			$this->IdStock = "";
			$this->IdVenta = "";
	
			
		}
    }

    public function insertar(){
        $arrUser = (array) $this;
        $this->insertRow("INSERT INTO StockVenta
            VALUES ('?', ?, ?)", array( 
            $this->IdStock,
            $this->IdVenta,
            ));
		$this->Disconnect();
    }

    public function editar(){
		
        return $this->user_login;
    }

    public function eliminar(){
        return $this->user_login;
    }

    public static function buscarForId($id){
		if ($id > 0){
			$st = new StockVenta();
            $getrow = $st->getRow("SELECT * FROM StockVenta WHERE IdStockVenta =?", array($id));
            $st->id = $getrow['IdStock'];
            
            $sto= new Stock();
            $sto->buscarForId($id)->getIdStock("IdStock");
            $st->setIdPedido($sto);

            $ven= new Venta();
            $ven->buscarForId($id)->getIdVenta("IdVenta");
            $st->setIdSuministro($ven);
         
                    
            $st->Disconnect();
			return $st;
		}else{
			return NULL;
		}

    }
	
	public static function getAll(){
    
        return Stock::buscar("SELECT * FROM StockVenta");
    }
    
    public static function buscar($query){
        $arraystockventa = array();
        $tmp = new StockVenta();
        $getrows = $tmp->getrows($query);
        
        foreach ($getrows as $valor) {
            $stockventa = new StockVenta();
            $stockventa->IdStockVenta = $valor['IdStockVenta'];
            $stockventa->Cantidad = $valor['IdStock'];
            $stockventa->IdPedido = $valor['IdVenta'];

            array_push($arraystockventa, $stockventa);
        }
        $tmp->Disconnect();
        return $arraystockventa;
    }

}
?>