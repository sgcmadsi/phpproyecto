 <?php
require_once('db_abstract_class.php');

class Proveedor extends db_abstract_class{
    
    private $IdProveedor;
    private $Nombre;
    private $Nit;
    private $Direccion;
    private $Telefono;
    private $Descripcion;
    private $Estado;
    private $Usuario;
    private $Contrasena;
    

    /* METODOS GET Y SET*/
    public function getIdProveedor(){
        return $this->IdProveedor;
    }
    private function setIdProveedor($IdProveedor){
        $this->IdProveedor = $IdProveedor;
        return $this;
    }



    public function getNombre(){
        return $this->Nombre;
    }
    private function setNombre ($Nombre){
        $this->Nombre = $Nombre;
        return $this;
    }



    public function getNit(){
        return $this->Nit;
    }
    private function setNit ($Nit){
        $this->Nit = $Nit;
        return $this;
    }



    public function getDireccion(){
        return $this->Direccion;
    }
    private function setDireccion ($Direccion){
        $this->Direccion = $Direccion;
        return $this;
    }



    public function getTelefono(){
        return $this->Telefono;
    }
    private function setTelefono($Telefono){
        $this->Telefono = $Telefono;
        return $this;
    }




    
    public function getDescripcion(){
        return $this->Descripcion;
    }
    private function setDescripcion($Descripcion){
        $this->Descripcion = $Descripcion;
        return $this;
    }



    public function getEstado(){
        return $this->Estado;
    }
    private function setEstado($Estado){
        $this->Estado = $Estado;
        return $this;
    }


    public function getUsuario(){
        return $this->Usuario;
    }
    private function setUsuario($Usuario){
        $this->Usuario = $Usuario;
        return $this;
    }



    public function getContrasena(){
        return $this->Contrasena;
    }
    private function setContrasena($Contrasena){
        $this->Contrasena = $Contrasena;
        return $this;
    }


    function __destruct(){
        $this->Disconnect();
    }

    public function __construct($user_data=array()){
        parent::__construct();
        if(count($user_data)>1){
            foreach ($user_data as $campo=>$valor){
                $this->$campo = $valor;
            }
        }else {
            $this->Nombre = "";
            $this->Nit = "";
            $this->Direccion = "";
            $this->Telefono = "";
            $this->Descripcion = "";
            $this->Estado = "";
            $this->Usuario = "";
            $this->Contrasena = "";
        }
    }

     public function insertar(){
        $arrProveedor = (array) $this;
        $this->insertRow("INSERT INTO Proveedor
            VALUES ('?', ?, ?, ?, ?, ?, ?, ?, ?)", array( 
                $this->Nombre,
                $this->Nit,
                $this->Direccion,
                $this->Telefono,
                $this->Descripcion,
                $this->Estado,
                $this->Usuario,
                $this->Contrasena,
            )
        );
        $this->Disconnect();
    }

   public function editar(){
        $arrProveedor = (array) $this;
        $this->updateRow("UPDATE Proveedor SET Nombre = ?, Nit = ?, Direccion = ?, Telefono = ?, Descripcion = ?, Estado = ? , Usuario = ?, Contrasena = ? WHERE IdProveedor = ?", array(
                $this->Nombre,
                $this->Nit,
                $this->Direccion,
                $this->Telefono,
                $this->Descripcion,
                $this->Estado,
                $this->Usuario,
                $this->Contrasena, 
                $this->IdProveedor, 
        ));
        $this->Disconnect();
    }

    public function eliminar(){
        return $this->IdProveedor;
    }


    public static function buscarForId($IdProveedor){
        if ($IdProveedor > 0){
            $prov = new Proveedor();
            $getrow = $prov->getRow("SELECT * FROM Proveedor WHERE IdProveedor =?", array($IdProveedor));
            $prov->IdProveedor = $getrow['IdProveedor'];
            $prov->Nombre = $getrow['Nombre'];
            $prov->Nit = $getrow['Nit'];
            $prov->Direccion = $getrow['Direccion'];
            $prov->Telefono = $getrow['Telefono'];
            $prov->Descripcion = $getrow['Descripcion'];
            $prov->Estado = $getrow['Estado'];

            $prov->Usuario = $getrow['Usuario'];
            $prov->Contrasena = $getrow['Contrasena'];
            $prov->IdProveedor = $getrow['IdProveedor'];

          

            $prov->Disconnect();
            return $prov;
        }else{
            return NULL;
        }
        $this->Disconnect();
    }
    
    public static function getAll(){
        return Proveedor::buscar("SELECT * FROM Proveedor");
    }

   // public static function getNombre(){
     //   return Proveedor::buscar("SELECT Nombre FROM Proveedor");
   // }
    
    public static function buscar($query){
        $arrProveedor = array();
        $tmp = new Proveedor();

        $getrows = $tmp->getrows($query);

        
        foreach ($getrows as $valor) {
            $prov = new Proveedor();
            $prov->IdProveedor = $valor['IdProveedor'];
            $prov->Nombre = $valor['Nombre'];
            $prov->Nit = $valor['Nit'];
            $prov->Direccion = $valor['Direccion'];
            $prov->Telefono = $valor['Telefono'];
            $prov->Descripcion = $valor['Descripcion'];
            $prov->Estado = $valor['Estado'];
            $prov->Usuario = $valor['Usuario'];
            $prov->Contrasena = $valor['Contrasena'];
            array_push($arrProveedor, $prov);
        }
        $tmp->Disconnect();
        return $arrProveedor;
    }

    public static function Login($Usuario,$Contrasena){
        if (!empty($Usuario) && !empty($Contrasena)){
            $prov = new Proveedor();
            $valor = $prov->getRow("SELECT * FROM Proveedor WHERE Usuario =? AND Contrasena =?", array($Usuario, $Contrasena));
            if($valor != false){
                $prov->Nombre = $valor['Nombre'];
                $prov->Nit = $valor['Nit'];
                $prov->Direccion = $valor['Direccion'];
                $prov->Telefono = $valor['Telefono'];
                $prov->Descripcion = $valor['Descripcion'];
                $prov->Estado = $valor['Estado'];
                $prov->Usuario = $valor['Usuario'];
                $prov->Contrasena = $valor['Contrasena'];
                $prov->Disconnect();
                return $prov;                
            }else{
                return NULL;
            }
        }else{
            return NULL;
        }
    }

}
?>