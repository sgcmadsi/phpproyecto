<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Ejemplo - PHP POO</title>
<?php require_once("snippets/includes_files.php"); ?>
</head>
<body>
    <div id="wrapper">

        <header>
            <div class="clearfix">
                <div class="clear"></div>

                <a class="chevron fr">Expand/Collapse</a>
                <nav>
                    <ul class="clearfix">
                       <li><a href="addsuministro.php" title="Nuevo Suministro"><img src="images/woofunction-icons/user_32.png" /><span>Nuevo</span></a></li>
                       <li><a href="versuministro.php" title="Buscar Suministros"><img src="images/woofunction-icons/search_button_32.png" /><span>Busqueda</span></a></li>
                        
                        
                        <li class="fr action">
                            <a href="documentation/index.html" class="button button-orange help" rel="#overlay"><span class="help"></span>Help</a>
                        </li>
                        <li class="fr action">
                            <a href="#" class="has-popupballoon button button-blue"><span class="add"></span>New Contact</a>
                            <div class="popupballoon bottom">
                                <h3>Add new contact</h3>
                                First Name<br />
                                <input type="text" /><br />
                                Last Name<br />
                                <input type="text" /><br />
                                Company<br />
                                <input type="text" />
                                <hr />
                                <button class="button button-orange">Add contact</button>
                                <button class="button button-gray close">Cancel</button>
                            </div>
                        </li>
                        <li class="fr action">
                            <a href="#" class="has-popupballoon button button-blue"><span class="add"></span>New Task</a>
                            <div class="popupballoon bottom">
                                <h3>Add new task</h3>
                                <input type="text" /><br /><br />
                                When it's due?<br />
                                <input type="date" /><br />
                                What category?<br />
                                <select><option>None</option></select>
                                <hr />
                                <button class="button button-orange">Add task</button>
                                <button class="button button-gray close">Cancel</button>
                            </div>
                        </li>
                        <li class="fr"><a href="#" class="arrow-down">administrator</a>
                            <ul>
                                <li><a href="#">Account</a></li>
                                <li><a href="#">Clientes</a></li>
                                <li><a href="#">Groups</a></li>
                                <li><a href="#">Sign out</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </header>
        <section>
            <div class="container_8 clearfix">                

                <!-- Main Section -->
                <section class="main-section grid_8">
                    <!-- Forms Section -->
                    <div class="main-content grid_5 alpha">
                        <header>
                            <h2>Registro de Suministros</h2>
                        </header>
                        <section class="clearfix">
                            <form class="form" action="controladores/suministro_controller.php?action=crear" method="POST">
                            <?php if (!empty($_GET['respuesta'])){ ?>
                                <?php if ($_GET['respuesta'] != "error") { ?>
                                <div class="message success closeable"><span class="message-close"></span>
                                    <h3>Correcto!</h3>
                                    <p>El Suministro se ha creado correctamente.</p>
                                </div>
                                <?php } else{ ?>
                                <div class="message error closeable"><span class="message-close"></span>
                                    <h3>Error!</h3>
                                    <p>El Suministro no ha sido Registrado.</p>
                                </div>
                                <?php } ?>
                            <?php } ?>
                                <div class="clearfix">
                                    <label>Nombre <em>*</em><small>Nombres del Suministro</small></label><input type="text" name="Nombre" id="Nombre" required="Nombre" maxlength="70" />
                                </div>
                                <div class="clearfix">
                                    <label>Descripcion <em>*</em><small>Descripcion del Suministro</small></label><textarea type="text" name="Descripcion" id="Descripcion" required="Descripcion" maxlength="300"></textarea>
                                </div>
                                <div class="clearfix">
                                    <label>Tipo <em>*</em><small>Tipo de Suministro</small></label>
                                    <select id="Tipo" name="Tipo">
                                    <option value="Dispositivo de Salida">Dispositivo de Salida</option>
                                    <option value="Dispositivo de Entrada">Dispositivo de Entrada</option>
                                    <option value="Otro">Otro</option>                                    
                                    </select>
                                </div>
                                <div class="clearfix">
                                    <label>Categoria <em>*</em><small>Categoria del Suministro</small></label><input type="text" name="Categoria" id="Categoria" required="Categoria" />
                                </div>
                                
                                
                                <div class="clearfix">
                                    <label>Marca <em>*</em><small>Marca del Suministro</small></label><input type="text" name="Marca" id="Marca" required="Marca" />
                                </div>

                                <div class="clearfix">
                                    <label>Referencia <em>*</em><small>Referencia del Suministro</small></label><input type="text" name="Referencia" id="Referencia" required="Referencia" />
                                </div>
                                
                                <div class="action clearfix">
                                    <button class="button button-gray" type="submit"><span class="accept"></span>OK</button>
                                    <button class="button button-gray" type="reset">Reset</button>
                                </div>
                            </form>
                        </section>
                    </div>
                    <!-- End Forms Section -->

                    <!-- Accordion Section -->
                    <div class="main-content grid_3 omega">
                        <header><h2>Instrucciones</h2></header>
                        <section class="accordion clearfix">
                            <header class="current"><h2>Datos del Usuario</h2></header>
                            <section style="display:block">
                                <h3>Nombres</h3>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                <h3>Where does it come from?</h3>
                                <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.</p>
                            </section>
                        </section>
                    </div>
                    <!-- End Accordion Section -->

                    <div class="clear"></div>

                </section>

                <?php require_once("snippets/footer.php"); ?>
                <!-- Main Section End -->

            </div>
        </section>
    </div>

</body>
</html>
