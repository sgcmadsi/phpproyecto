<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" charset="UTF-8">
<title>Actualizaci&oacute;n - Proveedores</title>
<?php
session_start();
require_once("snippets/includes_files.php"); 
require_once("clases/ClassProveedor.php"); 
?>
</head>
<body>
    <div id="wrapper">

        <header>
            <div class="clearfix">
                <div class="clear"></div>

                <a class="chevron fr">Expand/Collapse</a>
                <nav>
                    <ul class="clearfix">
                        <li><a href="addproveedor.php">Registrar</a></li>
                        <li ><a href="verproveedor.php">Buscar</a></li>
                        
                        <li class="fr action">
                            <a href="documentation/index.html" class="button button-orange help" rel="#overlay"><span class="help"></span>Help</a>
                        </li>
                        <li class="fr action">
                            <a href="#" class="has-popupballoon button button-blue"><span class="add"></span>New Contact</a>
                            <div class="popupballoon bottom">
                                <h3>Add new contact</h3>
                                First Name<br />
                                <input type="text" /><br />
                                Last Name<br />
                                <input type="text" /><br />
                                Company<br />
                                <input type="text" />
                                <hr />
                                <button class="button button-orange">Add contact</button>
                                <button class="button button-gray close">Cancel</button>
                            </div>
                        </li>
                        <li class="fr action">
                            <a href="#" class="has-popupballoon button button-blue"><span class="add"></span>New Task</a>
                            <div class="popupballoon bottom">
                                <h3>Add new task</h3>
                                <input type="text" /><br /><br />
                                When it's due?<br />
                                <input type="date" /><br />
                                What category?<br />
                                <select><option>None</option></select>
                                <hr />
                                <button class="button button-orange">Add task</button>
                                <button class="button button-gray close">Cancel</button>
                            </div>
                        </li>
                        <li class="fr"><a href="#" class="arrow-down">administrator</a>
                            <ul>
                                <li><a href="#">Account</a></li>
                                <li><a href="#">Cientes</a></li>
                                <li><a href="#">Groups</a></li>
                                <li><a href="#">Sign out</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </header>
        <section>
            <div class="container_8 clearfix">                

                <!-- Main Section -->
                <section class="main-section grid_8">
                    <!-- Forms Section -->
                    <div class="main-content grid_5 alpha">
                        <header>
                        <h2><b>Actualizaci&oacute;n de Proveedores</b></h2>
                        </header>
                        <section class="clearfix">
                        <form class="form" action="controladores/proveedor_controller.php?action=editar" method="POST">
                                                       
                            <?php 

                                $_SESSION['IdProveedor'] = $_GET['IdProveedor'];
                                $pro = new Proveedor();
                                $Proveedor = $pro->buscarForId($_SESSION['IdProveedor']);
                            ?>
                                
                                <div class="clearfix">
                                    <label>Nombre <em>*</em><small>Nombre del Proveedor</small></label><input type="text" name="Nombre" id="Nombre" required="Nombre" maxlength="40" value="<?php echo $Proveedor->getNombre(); ?>" />
                                </div>
                                <div class="clearfix">
                                    <label>Nit <em>*</em><small>Nit del Proveedor</small></label><input type="text" name="Nit" id="Nit" required="Nit" value="<?php echo $Proveedor->getNit(); ?>"/>
                                </div>
                                
                                <div class="clearfix">
                                    <label>Direcci&oacute;n <em>*</em><small>Lugar de Residencia</small></label><input type="text" name="Direccion" id="Direccion" required="Direccion" maxlength="25" value="<?php echo $Proveedor->getDireccion(); ?>"/>
                                </div>

                                <div class="clearfix">
                                    <label>Telefono <em>*</em><small>Telefono del Proveedor</small></label><input type="text" name="Telefono" id="Telefono" required="Telefono" maxlength="50" value="<?php echo $Proveedor->getTelefono(); ?>" />
                                </div>

                                 <div class="clearfix">
                                    <label>Descripcion <em>*</em><small>Descripcion del Proveedor</small></label><input type="text" name="Descripcion" id="Descripcion" required="Descripcion" maxlength="300" value="<?php echo $Proveedor->getDescripcion(); ?>"/>
                                </div>

                                <div class="clearfix">
                                    <label>Estado <em>*</em><small>Estado del Cliente</small></label>
                                    <select id="Estado" name="Estado">
                                    <option value="Activo">Activo</option>
                                    <option value="Inactivo">Inactivo</option>
                                    <option value="<?php echo $Proveedor->getEstado(); ?>"selected><?php echo $Proveedor->getEstado(); ?></option>
                                    </select>
                                </div>

                                <div class="clearfix"> 
                                    <label>Usuario <em>*</em><small>Ingrese su Usuario</small></label><input type="text" name="Usuario" id="Usuario" required="Usuario" value="<?php echo $Proveedor->getUsuario(); ?>" />
                                </div>
                                <div class="clearfix">
                                    <label>Password <em>*</em><small>Ingrese su Password</small></label><input type="password" name="Contrasena" id="Contrasena" required="Contrasena" maxlength="20" value="<?php echo $Proveedor->getContrasena(); ?>" />
                                </div>
                                                             
                                <div class="action clearfix">
                                    <button class="button button-gray" type="submit"><span class="accept"></span>OK</button>
                                    <button class="button button-gray" type="reset">Reset</button>
                                </div>
                            </form>
                         
                        </section>
                    </div>


                    <!-- End Forms Section -->

                    <!-- Accordion Section -->
                    <div class="main-content grid_3 omega">
                        <header><h2>Instrucciones</h2></header>
                        <section class="accordion clearfix">
                            <header class="current"><h2>Datos del Usuario</h2></header>
                            <section style="display:block">
                                <h3>Nombres</h3>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                <h3>Where does it come from?</h3>
                                <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.</p>
                            </section>
                        </section>
                    </div>
                    <!-- End Accordion Section -->

                    <div class="clear"></div>

                </section>

                <?php require_once("snippets/footer.php"); ?>
                <!-- Main Section End -->

            </div>
        </section>
    </div>

</body>
</html>
