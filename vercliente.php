<?php require_once "controladores/cliente_controller.php"; 

//require_once(__DIR__.'/clases/ClassCliente.php');


?>
<!DOCTYPE html> 
<html lang="en">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title>Ver - Clientes</title>
<?php require_once("snippets/includes_files.php"); ?>
</head>
<body>
    <div id="wrapper">

        <header>
            <div class="clearfix">
                <div class="clear"></div>

                <a class="chevron fr">Expand/Collapse</a>
                <nav>
                    <ul class="clearfix">
                       <li><a href="addcliente.php" title="Nuevo Cliente"><img src="images/woofunction-icons/user_32.png" /><span>Nuevo</span></a></li>
                       <li><a href="vercliente.php" title="Buscar Cliente"><img src="images/woofunction-icons/search_button_32.png" /><span>Busqueda</span></a></li>
                        
                        
                        <li class="fr action">
                            <a href="documentation/index.html" class="button button-orange help" rel="#overlay"><span class="help"></span>Help</a>
                        </li>
                        <li class="fr action">
                            <a href="#" class="has-popupballoon button button-blue"><span class="add"></span>New Contact</a>
                            <div class="popupballoon bottom">
                                <h3>Add new contact</h3>
                                First Name<br />
                                <input type="text" /><br />
                                Last Name<br />
                                <input type="text" /><br />
                                Company<br />
                                <input type="text" />
                                <hr />
                                <button class="button button-orange">Add contact</button>
                                <button class="button button-gray close">Cancel</button>
                            </div>
                        </li>
                        <li class="fr action">
                            <a href="#" class="has-popupballoon button button-blue"><span class="add"></span>New Task</a>
                            <div class="popupballoon bottom">
                                <h3>Add new task</h3>
                                <input type="text" /><br /><br />
                                When it's due?<br />
                                <input type="date" /><br />
                                What category?<br />
                                <select><option>None</option></select>
                                <hr />
                                <button class="button button-orange">Add task</button>
                                <button class="button button-gray close">Cancel</button>
                            </div>
                        </li>
                        <li class="fr"><a href="#" class="arrow-down">administrator</a>
                            <ul>
                                <li><a href="#">Account</a></li>
                                <li><a href="#">Clientes</a></li>
                                <li><a href="#">Groups</a></li>
                                <li><a href="#">Sign out</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </header>
        <section>
            <div class="container_8 clearfix"> 
                   
                            
                <!-- Main Section -->
                <section class="main-section grid_8">
                    <?php if (!empty($_GET['respuesta'])){ ?>
                                <?php if ($_GET['respuesta'] != "error") { ?>
                                <div class="message success closeable"><span class="message-close"></span>
                                    <h3>Correcto!</h3>
                                    <p>El Cliente se ha Modificado correctamente.</p>
                                </div>
                                <?php } else{ ?>
                                <div class="message error closeable"><span class="message-close"></span>
                                    <h3>Error!</h3>
                                    <p>El Cliente no se ha Modificado.</p>
                                </div>
                                <?php } ?>
                            <?php } ?>

                    <!-- Tables Section -->
                    <div class="main-content">
                        <header>
                            <input type="text" class="search fr" placeholder="Search..."/>
                            <h2><b>Busqueda de Clientes</b></h2>
                        </header>
                        <section class="with-table">  
                          
                        <table class="datatable tablesort selectable paginate full">
                                <thead>
                                    <tr>
                                        <th style="width: 50px">Cod</th>
                                        <th style="width: 110px">Tipo Documento</th>
                                        <th>Documento</th>
                                        <th>Nombres</th>
                                        <th>Apellidos</th>
                                        <th>Direccion</th>
                                        <th>Telefono</th>
                                        <th style="width: 80px">Estado</th>
                                        <th style="width: 80px">Acciones</th>
                                    </tr>
                                </thead>
                               <tbody>
                                   <?php echo usuarios_controller::vercliente(); ?> 
                                </tbody>

                            </table>                                        
                        </section>
                    </div>
                    <!-- End Tables Section -->

                    <div class="clear"></div>

                </section>

                <?php require_once("snippets/footer.php");?>
                <!-- Main Section End -->

            </div>
        </section>
    </div>

</body>
</html>
