<?php

require_once ('/../clases/ClassVenta.php');


session_start();
if(!empty($_GET['action'])){
	venta_controller::main($_GET['action']);
}

class venta_controller{
	
	static function main($action){
		if ($action == "crear"){
			venta_controller::crear();
		}else if ($action == "editar"){
			venta_controller::editar();
		}else if ($action == "buscarID"){
			venta_controller::buscarID(1);
		}else if($action == "verventa"){
			venta_controller::verventa();

		}

	}
	
	static public function crear(){
		try {
			$arrayVenta = array();
			$arrayVenta['Fecha'] = $_POST['Fecha'];
			$arrayVenta['Cantidad'] = $_POST['Cantidad'];
			$arrayVenta['ModoPago'] = $_POST['ModoPago'];
			$arrayVenta['NumeroVenta'] = $_POST['NumeroVenta'];
			$arrayVenta['Total'] = $_POST['Total'];
			$arrayVenta['IdCliente'] = $_SESSION['IdCliente'];
			$venta = new Venta ($arrayVenta);
			$venta->insertar();
			header("Location: ../verventa.php?respuesta=correcto");
		} catch (Exception $e) {
			header("Location: ../addventa.php?respuesta=error"); 

		}
	}
	
	static public function editar (){
		try {
			$arrayVenta = array();
			$arrayVenta['Fecha'] = $_POST['Fecha'];
			$arrayVenta['Cantidad'] = $_POST['Cantidad'];
			$arrayVenta['NumeroVenta'] = $_POST['NumeroVenta'];
			$arrayVenta['ModoPago'] = $_POST['ModoPago'];
			$arrayVenta['Total'] = $_POST['Total'];
			$arrayVenta['IdCliente'] = $_POST['IdCliente'];
			$arrayVenta['IdVenta'] = $_POST['IdVenta'];
			var_dump($arrayVenta);
			$usuario = new Venta ($arrayVenta);
			$usuario->editar();
			header("Location: ../editar.php?respuesta=correcto");
		} catch (Exception $e) {
			header("Location: ../editar.php?respuesta=error");
		}
	}

	static public function buscarID ($IdVenta){
		try { 
			return Venta::buscarForId($IdVenta);
		} catch (Exception $e) {
			header("Location: ../buscar.php?respuesta=error");
		}
	}
	

	public function buscarAll (){
		try {
			return Venta::getAll();
		} catch (Exception $e) {
			header("Location: ../buscar.php?respuesta=error");
		}
	}

	static public function verventa(){
		try { 
			$ven = new Venta();
			$arrayVenta = $ven::getAll();
			if (count($arrayVenta > 0)) {
				foreach ($arrayVenta as $vent) {
					echo "<tr>";
					echo "<td>".$vent->getIdVenta()."</td>";
					echo "<td>".$vent->getFecha()."</td>";
					echo "<td>".$vent->getCantidad()."</td>";
					echo "<td>".$vent->getModoPago()."</td>";
					echo "<td>".$vent->getNumeroVenta()."</td>";
					echo "<td>".$vent->getTotal()."</td>";
					echo "<td>".$vent->getIdCliente()."</td>";
					
					//echo "<td> <a href='formactualizarcliente.php?IdCliente=".$clien->getIdCliente()."'><img src='images/icons/application_form_edit.png'/></a></td>";
                    echo "</tr>";
				}
			}else{
				echo "No existen Clientes Registrados";
			}		
		} catch (Exception $e) {
			
		}
	}


	public static function getListVenta($nameList){
		try { 
			$venta = new Venta();
			$arrayVenta = $venta::getAll();
			if (count($arrayVenta>0)) {
				echo "<select id='".$nameList."' name='".$nameList."'>";
				foreach ($arrayVenta as $ven) {		
        			echo "  <option value='".$ven->getIdVenta()."'>".$ven->getFecha()." </option>";
				}
				echo "</select><br/>";
			}else{
				echo "No existen Clientes Registrados";
			}		
		} catch (Exception $e) {
			echo $e;
		}
	}
}
?>