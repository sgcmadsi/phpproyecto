
|<?php
session_start();
//importar clases

require_once(__DIR__.'/../clases/ClassCliente.php');
require_once(__DIR__.'/../clases/ClassProveedor.php');

if(!empty($_GET['action'])){
	usuarios_controller::main($_GET['action']);
}

class usuarios_controller{
	
	static function main($action){
		if ($action == "crear"){
			usuarios_controller::crear();
		}else if ($action == "editar"){
			usuarios_controller::editar();
		}else if ($action == "buscarID"){
			usuarios_controller::buscarID(1);
		}else if ($action == "vercliente") {
			usuarios_controller::vercliente();
		}else if ($action == "login"){
			usuarios_controller::login();
		}else if ($action == "eliminar"){
			usuarios_controller::eliminar();
		}
	}
	
	static public function crear (){
		try {
			$arrCliente = array();
			$arrCliente['TipoDocumento'] = $_POST['TipoDocumento'];
			$arrCliente['Documento'] = $_POST['Documento'];
			$arrCliente['Nombres'] = $_POST['Nombres'];
			$arrCliente['Apellidos'] = $_POST['Apellidos'];
			$arrCliente['Direccion'] = $_POST['Direccion'];
			$arrCliente['Telefono'] = $_POST['Telefono'];
			$arrCliente['Estado'] = $_POST['Estado'];
			$arrCliente['Usuario'] = $_POST['Usuario'];
			$arrCliente['Contrasena'] = $_POST['Contrasena'];
			$cliente = new Cliente($arrCliente);
			$cliente->insertar();
			header("Location: ../vercliente.php?respuesta=correcto");
		} catch (Exception $e) {
			header("Location: ../vercliente.php?respuesta=error");
		}
	}
	
	static public function editar (){
		try {
			$arrCliente = array();
			$arrCliente['IdCliente'] = $_SESSION['IdCliente'];
			$arrCliente['TipoDocumento'] = $_POST['TipoDocumento'];
			$arrCliente['Documento'] = $_POST['Documento'];
			$arrCliente['Nombres'] = $_POST['Nombres'];
			$arrCliente['Apellidos'] = $_POST['Apellidos'];
			$arrCliente['Direccion'] = $_POST['Direccion'];
			$arrCliente['Telefono'] = $_POST['Telefono'];
			$arrCliente['Estado'] = $_POST['Estado'];
			$arrCliente['Usuario'] = $_POST['Usuario'];
			$arrCliente['Contrasena'] = $_POST['Contrasena'];

			//$arrCliente['IdCliente'] = $_GET['IdCliente'];
			
			$cliente = new Cliente ($arrCliente);
			$result = $cliente->editar();
			var_dump($result);

			$arrCliente['IdCliente'] = $_GET['IdCliente'];
			//var_dump($arrCliente);
			$cliente = new Cliente ($arrCliente);
			$cliente->editar();

			header("Location: ../vercliente.php?respuesta=correcto");
		} catch (Exception $e) {
			header("Location: ../vercliente.php?respuesta=error");
		}
	}
	/*
	static public function buscarID ($IdCliente){
		try { 
			return Cliente::buscarForId($IdCliente);
		} catch (Exception $e) {
			header("Location: ../buscar.php?respuesta=error");
		}
	}
	*/
	/*
	public function buscarAll (){
		try {
			return Cliente::getAll();
		} catch (Exception $e) {
			header("Location: ../buscar.php?respuesta=error");
		}
	}
*/
	static public function vercliente(){
		try { 
			$cliente = new Cliente();
			$arrCliente = $cliente::getAll();
			if (count($arrCliente>0)) {
				foreach ($arrCliente as $clien) {
					echo "<tr>";
					echo "<td>".$clien->getIdCliente()."</td>";
					echo "<td>".$clien->getTipoDocumento()."</td>";
					echo "<td>".$clien->getDocumento()."</td>";
					echo "<td>".$clien->getNombres()."</td>";
					echo "<td>".$clien->getApellidos()."</td>";
					echo "<td>".$clien->getDireccion()."</td>";
					echo "<td>".$clien->getTelefono()."</td>";
					echo "<td>".$clien->getEstado()."</td>";

					echo "<td> <a href='formactualizarcliente.php?IdCliente=".$clien->getIdCliente()."'><img src='images/icons/application_form_edit.png'/></a></td>";

					
                    echo "</tr>";
				}
			}else{
				echo "No existen Clientes Registrados";
			}		
		} catch (Exception $e) {
			
		}
	}



	public function eliminar(){
		try {
			$IdCliente = $_GET['IdCliente'];
			$Cliente = new Cliente ();
			$Cliente->eliminar($IdCliente);
			header("Location: ../vercliente.php");
		} catch (Exception $e) {
			echo $e;
			header("Location: ../vercliente.php");
		}
	}



	//metodo para cargar cliente en el combo de ventas
	public static function getListClientes($nameList){
		try { 
			$cliente = new Cliente();
			$arrCliente = $cliente::getAll();
			if (count($arrCliente>0)) {
				echo "<select id='".$nameList."' name='".$nameList."'>";
				foreach ($arrCliente as $clien) {		
        			echo "  <option value='".$clien->getIdCliente()."'>".$clien->getNombres()."  ".$clien->getApellidos()."</option>";
				}
				echo "</select><br/>";
			}else{
				echo "No existen Clientes Registrados";
			}		
		} catch (Exception $e) {
			echo $e;
		}
	}


    public function login(){
    	$Usuario=$_POST['Usuario'];
		$Contrasena=$_POST['Contrasena'];
		$TipoUser = $_POST['TipoUsuario'];
		$result = NULL;
		if($TipoUser == "Proveedor"){
			$result = Proveedor::login($Usuario, $Contrasena);	
		}else{
			$result = Cliente::login($Usuario, $Contrasena);
		}

		if($result != NULL){
			if ($TipoUser == "Proveedor"){
				header("Location: ../addsuministro.php");
			}else{
				header("Location: ../addventa.php");
			}
		}else{
			header("Location: ../login.php?Error='Datos Incorrectos'");
		}
    }
}


?>