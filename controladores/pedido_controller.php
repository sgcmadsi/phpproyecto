<?php
session_start();
require_once(__DIR__.'/../clases/ClassPedido.php');
require_once(__DIR__.'/../clases/ClassProveedor.php');
require_once(__DIR__.'/../controladores/proveedor_controller.php');


if(!empty($_GET['action'])){
	pedido_controller::main($_GET['action']);
}

class pedido_controller{
	
	static function main($action){
		if ($action == "crear"){
			pedido_controller::crear();
		}else if ($action == "editar"){
			pedido_controller::editar();
		}else if ($action == "buscarID"){
			pedido_controller::buscarID(1);
		}elseif ($action == "verpedido") {
			pedido_controller::verpedido();
		}
	}
	
	static public function crear(){
		try {
			$arrayPedido = array();
			$arrayPedido['FechaRecibido'] = $_POST['FechaRecibido'];
			$arrayPedido['FechaEntrega'] = $_POST['FechaEntrega'];
			$arrayPedido['FormaPago'] = $_POST['FormaPago'];
			$arrayPedido['Estado'] = $_POST['Estado'];
			$arrayPedido['Solicitante'] = $_POST['Solicitante'];
			$arrayPedido['IdProveedor'] = $_SESSION['IdProveedor'];
			$pedido = new Pedido ($arrayPedido);
			$pedido->insertar();
			header("Location: ../verpedido.php?respuesta=correcto");
		} catch (Exception $e) {
			header("Location: ../addpedido.php?respuesta=error");
		}
	}
	
	static public function editar (){
		try {
			$arrayPedido = array();
			$arrayPedido['FechaRecibido'] = $_POST['FechaRecibido'];
			$arrayPedido['FechaEntrega'] = $_POST['FechaEntrega'];
			$arrayPedido['FormaPago'] = $_POST['FormaPago'];
			$arrayPedido['Estado'] = $_POST['Estado'];
			$arrayPedido['Solicitante'] = $_POST['Solicitante'];
			$arrayPedido['IdProveedor'] = $_POST['IdProveedor'];
			$arrayPedido['IdPedido'] = $_POST['IdPedido'];
			var_dump($arrayPedido);
			$pedido = new Pedido ($arrayPedido);
			$pedido->editar();
			header("Location: ../editar.php?respuesta=correcto");
		} catch (Exception $e) {
			header("Location: ../editar.php?respuesta=error");
		}
	}
	
	/*static public function buscarID ($IdPedido){
		try { 
			return Pedido::buscarForId($IdPedido);
		} catch (Exception $e) {
			header("Location: ../buscar.php?respuesta=error");
		}
	}
	
	public function buscarAll (){
		try {
			return Pedido::getAll();
		} catch (Exception $e) {
			header("Location: ../buscar.php?respuesta=error");
		}
	}

	public function buscar ($campo, $parametro){
		try {
			return Pedido::getAll();
		} catch (Exception $e) {
			header("Location: ../buscar.php?respuesta=error");
		}
	}

*/
	static public function verpedido(){
		try { 
			$pedido = new Pedido();
			$arrayPedido = $pedido::getAll();
			if (count($arrayPedido>0)) {
				foreach ($arrayPedido as $ped) {
					echo "<tr>";
					echo "<td>".$ped->getIdPedido()."</td>";
					echo "<td>".$ped->getFechaRecibido()."</td>";
					echo "<td>".$ped->getFechaEntrega()."</td>";
					echo "<td>".$ped->getFormaPago()."</td>";
					echo "<td>".$ped->getEstado()."</td>";
					echo "<td>".$ped->getSolicitante()."</td>";
					echo "<td>".$ped->getIdProveedor()."</td>";
					echo "</tr>";

				}
			}else{
				echo "No existen Proveedores Registrados";
			}

			
		} catch (Exception $e) {
			
		}
	}



	public static function getListPedido($nameList){
		try { 
			$pedi = new Pedido();
			$arrayPedido = $pedi::getAll();
			if (count($arrayPedido>0)) {
				echo "<select id='".$nameList."' name='".$nameList."'>";
				foreach ($arrayPedido as $ped) {		
        			echo "  <option value='".$ped->getIdPedido()."'>".$ped->getSolicitante()." / ".$ped->getFechaRecibido()."</option>";
				}
				echo "</select><br/>";
			}else{
				echo "No existen Clientes Registrados";
			}		
		} catch (Exception $e) {
			echo $e;
		}
	}
	
}
?>