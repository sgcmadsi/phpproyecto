<?php

session_start();

require_once(__DIR__.'/../clases/ClassProveedor.php');
require_once(__DIR__.'/../clases/ClassCliente.php');

if(!empty($_GET['action'])){
	proveedor_controller::main($_GET['action']);
}

class proveedor_controller{
	
	static function main($action){
		if ($action == "crear"){
			proveedor_controller::crear();
		}else if ($action == "editar"){
			proveedor_controller::editar();
		}else if ($action == "buscarID"){
			proveedor_controller::buscarID(1);
		}else if ($action == "verproveedor") {
			proveedor_controller::verproveedor();
		}else if ($action == "login"){
			proveedor_controller::login();
		}
	}
	
	static public function crear (){
		try {
			$arrayProveedor = array();
			$arrayProveedor['Nombre'] = $_POST['Nombre'];
			$arrayProveedor['Nit'] = $_POST['Nit'];
			$arrayProveedor['Direccion'] = $_POST['Direccion'];
			$arrayProveedor['Telefono'] = $_POST['Telefono'];
			$arrayProveedor['Descripcion'] = $_POST['Descripcion'];
			$arrayProveedor['Estado'] = $_POST['Estado'];
			$arrayProveedor['Usuario'] = $_POST['Usuario'];
			$arrayProveedor['Contrasena'] = $_POST['Contrasena'];
			$proveedor = new Proveedor($arrayProveedor);
			$proveedor->insertar();
			header("Location: ../verproveedor.php?respuesta=correcto");
		} catch (Exception $e) {
			header("Location: ../verproveedor.php?respuesta=error");

		}
	}
	
	static public function editar(){
		try {
			$arrayProveedor = array();
			$arrayProveedor['IdProveedor'] = $_SESSION['IdProveedor'];
			$arrayProveedor['Nombre'] = $_POST['Nombre'];
			$arrayProveedor['Nit'] = $_POST['Nit'];
			$arrayProveedor['Direccion'] = $_POST['Direccion'];
			$arrayProveedor['Telefono'] = $_POST['Telefono'];
			$arrayProveedor['Descripcion'] = $_POST['Descripcion'];
			$arrayProveedor['Estado'] = $_POST['Estado'];
			$arrayProveedor['Usuario'] = $_POST['Usuario'];
			$arrayProveedor['Contrasena'] = $_POST['Contrasena'];
			//$arrayProveedor['IdProveedor'] = $_GET['IdProveedor'];
			$proveedor = new Proveedor ($arrayProveedor);
			$result = $proveedor->editar();
			var_dump($result);
			header("Location: ../verproveedor.php?respuesta=correcto");
		} catch (Exception $e) {
			header("Location: ../verproveedor.php?respuesta=error");
		}
	}
	
	static public function buscarID ($id){
		try { 
			return Proveedor::buscarForId($id);
		} catch (Exception $e) {
			header("Location: ../buscar.php?respuesta=error");
		}
	}
	
	public function buscarAll (){
		try {
			return Proveedor::getAll();
		} catch (Exception $e) {
			header("Location: ../buscar.php?respuesta=error");
		}
	}




	static public function verproveedor(){
		try { 
			$proveedor = new Proveedor();
			$arrayProveedor = $proveedor::getAll();
			if (count($arrayProveedor>0)) {
				foreach ($arrayProveedor as $prov) {
					echo "<tr>";
					echo "<td>".$prov->getIdProveedor()."</td>";
					echo "<td>".$prov->getNombre()."</td>";
					echo "<td>".$prov->getNit()."</td>";
					echo "<td>".$prov->getDireccion()."</td>";
					echo "<td>".$prov->getTelefono()."</td>";
					echo "<td>".$prov->getDescripcion()."</td>";
					echo "<td>".$prov->getEstado()."</td>";

					echo "<td> <a href='formactualizarproveedor.php?IdProveedor=".$prov->getIdProveedor()."'><img src='images/icons/application_form_edit.png'/></a></td>";
                    echo "</tr>";

					echo "<td></td>";
					echo "</tr>";


				}
			}else{
				echo "No existen Proveedores Registrados";
			}

			
		} catch (Exception $e) {
			
		}
	}



	/*static public function vernombreproveedor(){
		try { 
			$proveedor = new Proveedor();
			$arrayProveedor = $proveedor::getNombre();
			if (count($arrayProveedor>0)) {
				foreach ($arrayProveedor as $prov) {
					echo "<tr>";
					echo "<td>".$prov->getIdProveedor()."</td>";
					echo "<td>".$prov->getNombre()."</td>";
					echo "<td>".$prov->getNit()."</td>";
					echo "<td>".$prov->getDireccion()."</td>";
					echo "<td>".$prov->getTelefono()."</td>";
					echo "<td>".$prov->getDescripcion()."</td>";
					echo "<td>".$prov->getEstado()."</td>";		
                    echo "</tr>";

				}
			}else{
				echo "No existen Proveedores Registrados";
			}

			
		} catch (Exception $e) {
			
		}
	}*/

	public static function getListProveedor($nameList){
		try { 
			$prove = new Proveedor();
			$arrayProveedor = $prove::getAll();
			if (count($arrayProveedor>0)) {
				echo "<select id='".$nameList."' name='".$nameList."'>";
				foreach ($arrayProveedor as $prov) {		
        			echo "  <option value='".$prov->getIdProveedor()."'>".$prov->getNombre()." ".$prov->getNit()."</option>";
				}
				echo "</select><br/>";
			}else{
				echo "No existen Proveedores Registrados";
			}		
		} catch (Exception $e) {
			echo $e;
		}
	}

	//public function eliminar(){
        

/*	public function eliminar(){
        $conexion = mysql_connect("localhost","Victor","Victor");
        if (!$conexion) {
            die ("No se puede conectar".mysql_error());
        }
        //selecciono la bases de datos
        mysql_select_db("Computadores",$conexion);
        $IdProveedor = @$_GET['IdProveedor'];
        //query
        $borrar= mysql_query("delete from Proveedor where IdProveedor= $IdProveedor");
        if ($borrar==true) {
        header("Location: ../verproveedor.php ");
         
        }else{
        header("Location: ../verproveedor.php ");
        }

        mysql_close($conexion);

    }	*/

    public function login(){
    	$Usuario=$_POST['Usuario'];
		$Contrasena=$_POST['Contrasena'];
		$TipoUser = $_POST['TipoUsuario'];
		$result = NULL;
		if($TipoUser == "Proveedor"){
			$result = Proveedor::login($Usuario, $Contrasena);	
		}else{
			$result = Cliente::login($Usuario, $Contrasena);
		}

		if($result != NULL){
			if ($TipoUser == "Proveedor"){
				header("Location: ../addsuministro.php");
			}else{
				header("Location: ../addventa.php");
			}
		}else{
			header("Location: ../login.php?Error='Datos Incorrectos'");
		}
    }
}

?>