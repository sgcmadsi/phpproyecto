|<?php
session_start();
//importar clases
require_once(__DIR__.'/../clases/ClassStock.php');

if(!empty($_GET['action'])){
	stock_controller::main($_GET['action']);
}

class stock_controller{
	
	static function main($action){
		if ($action == "crear"){
			stock_controller::crear();
		}else if ($action == "editar"){
			stock_controller::editar();
		}else if ($action == "buscarID"){
			stock_controller::buscarID(1);
		}else if ($action == "vercliente") {
			stock_controller::vercliente();
		}else if ($action == "login"){
			stock_controller::login();
		}else if ($action == "eliminar"){
			stock_controller::eliminar();
		}
	}
	
	static public function crear (){
		try {
			$arraystock = array();
			$arraystock['Cantidad'] = $_POST['Cantidad'];
			$arraystock['ValorUnitario'] = $_POST['ValorUnitario'];
			$arraystock['ValorVenta'] = $_POST['ValorVenta'];
			$arraystock['IdPedido'] = $_POST['IdPedido'];
			$arraystock['IdSuministro'] = $_POST['IdSuministro'];
			$stock = new Stock($arraystock);
			$stock->insertar();
			header("Location: ../verstock.php?respuesta=correcto");
		} catch (Exception $e) {
			header("Location: ../verstock.php?respuesta=error");
		}
	}
	
	static public function editar (){
		try {
			$arrCliente = array();
			$arrCliente['IdCliente'] = $_SESSION['IdCliente'];
			$arrCliente['TipoDocumento'] = $_POST['TipoDocumento'];
			$arrCliente['Documento'] = $_POST['Documento'];
			$arrCliente['Nombres'] = $_POST['Nombres'];
			$arrCliente['Apellidos'] = $_POST['Apellidos'];
			$arrCliente['Direccion'] = $_POST['Direccion'];
			$arrCliente['Telefono'] = $_POST['Telefono'];
			$arrCliente['Estado'] = $_POST['Estado'];
			$arrCliente['Usuario'] = $_POST['Usuario'];
			$arrCliente['Contrasena'] = $_POST['Contrasena'];
			//$arrCliente['IdCliente'] = $_GET['IdCliente'];
			
			$cliente = new Cliente ($arrCliente);
			$result = $cliente->editar();
			var_dump($result);
			header("Location: ../vercliente.php?respuesta=correcto");
		} catch (Exception $e) {
			header("Location: ../vercliente.php?respuesta=error");
		}
	}
	
	static public function buscarID ($IdStock){
		try { 
			return Stock::buscarForId($IdStock);
		} catch (Exception $e) {
			header("Location: ../buscar.php?respuesta=error");
		}
	}
	

	public function buscarAll (){
		try {
			return Stock::getAll();
		} catch (Exception $e) {
			header("Location: ../buscar.php?respuesta=error");
		}
	}

	static public function verstock(){
		try { 
			$stock = new Stock();
			$arraystock = $stock::getAll();
			if (count($arraystock>0)) {
				foreach ($arraystock as $st) {
					echo "<tr>";
					echo "<td>".$st->getIdStock()."</td>";
					echo "<td>".$st->getCantidad()."</td>";
					echo "<td>".$st->getValorUnitario()."</td>";
					echo "<td>".$st->getValorVenta()."</td>";
					echo "<td>".$st->getIdPedido()."</td>";
					echo "<td>".$st->getIdSuministro()."</td>";
					echo "</tr>";
				}
			}else{
				echo "No existen Clientes Registrados";
			}		
		} catch (Exception $e) {
			
		}
	}
/*
	public function eliminar(){
		try {
			$IdCliente = $_GET['IdCliente'];
			$Cliente = new Cliente ();
			$Cliente->eliminar($IdCliente);
			header("Location: ../vercliente.php");
		} catch (Exception $e) {
			echo $e;
			header("Location: ../vercliente.php");
		}
	}
*/

	public static function getListStock($nameList){
		try { 
			$stock = new Stock();
			$arraystock = $stock::getAll();
			if (count($arraystock>0)) {
				echo "<select id='".$nameList."' name='".$nameList."'>";
				foreach ($arraystock as $st) {		
        			echo "  <option value='".$st->getIdStock()."'>".$st->getValorUnitario()." </option>";
				}
				echo "</select><br/>";
			}else{
				echo "No existen Clientes Registrados";
			}		
		} catch (Exception $e) {
			echo $e;
		}
	}

}


?>