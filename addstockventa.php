<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Registro - Stock de Ventas</title>
<?php require_once("snippets/includes_files.php"); ?>
<?php require_once("controladores/stock_controller.php"); ?>
<?php require_once("controladores/venta_controller.php"); ?>
<?php // require_once("controladores/suministro_controller.php"); ?>

<body>
    <div id="wrapper">

                <header>
            <div class="clearfix">
                <div class="clear"></div>

                <a class="chevron fr">Expand/Collapse</a>
                <nav>
                    <ul class="clearfix">
                        <li><a href="addstockventa.php" title="Nuevo StockVenta"><img src="images/woofunction-icons/user_32.png" /><span>Nuevo</span></a></li>
                        <li><a href="verstockventa.php" title="Buscar StockVenta"><img src="images/woofunction-icons/search_button_32.png" /><span>Busqueda</span></a></li>
                        
                        
                        <li class="fr action">
                            <a href="documentation/index.html" class="button button-orange help" rel="#overlay"><span class="help"></span>Help</a>
                        </li>
                        <li class="fr action">
                            <a href="#" class="has-popupballoon button button-blue"><span class="add"></span>New Contact</a>
                            <div class="popupballoon bottom">
                                <h3>Add new contact</h3>
                                First Name<br />
                                <input type="text" /><br />
                                Last Name<br />
                                <input type="text" /><br />
                                Company<br />
                                <input type="text" />
                                <hr />
                                <button class="button button-orange">Add contact</button>
                                <button class="button button-gray close">Cancel</button>
                            </div>
                        </li>
                        <li class="fr action">
                            <a href="#" class="has-popupballoon button button-blue"><span class="add"></span>New Task</a>
                            <div class="popupballoon bottom">
                                <h3>Add new task</h3>
                                <input type="text" /><br /><br />
                                When it's due?<br />
                                <input type="date" /><br />
                                What category?<br />
                                <select><option>None</option></select>
                                <hr />
                                <button class="button button-orange">Add task</button>
                                <button class="button button-gray close">Cancel</button>
                            </div>
                        </li>
                        <li class="fr"><a href="#" class="arrow-down">administrator</a>
                            <ul>
                                <li><a href="#">Account</a></li>
                                <li><a href="#">Cientes</a></li>
                                <li><a href="#">Groups</a></li>
                                <li><a href="#">Sign out</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </header>
        <section>
            <div class="container_8 clearfix">                

                <!-- Main Section -->
                <section class="main-section grid_8">
                    <!-- Forms Section -->
                    <div class="main-content grid_5 alpha">
                        <header>
                            <h2>Registro de Stocks</h2>
                        </header>
                        <section class="clearfix">
                            <form class="form" action="controladores/stockventa_controller.php?action=crear" method="POST">
                            <?php if (!empty($_GET['respuesta'])){ ?>
                                <?php if ($_GET['respuesta'] != "error") { ?>
                                <div class="message success closeable"><span class="message-close"></span>
                                    <h3>Correcto!</h3>
                                    <p>El Stock se ha creado correctamente.</p>
                                </div>
                                <?php } else{ ?>
                                <div class="message error closeable"><span class="message-close"></span>
                                    <h3>Error!</h3>
                                    <p>El Stock no ha sido creado.</p>
                                </div>
                                <?php } ?>
                            <?php } ?>
                                
                                
                                <div class="clearfix">
                                    <label>Stock <em>*</em><small>Datos Stock</small></label>
                                    
                                    <?php echo stock_controller::getListStock("IdStock"); ?>
                                </div>

                                <div class="clearfix">
                                    <label>Venta <em>*</em><small>Datos de Venta</small></label>
                                    <?php echo venta_controller::getListVenta("IdVenta"); ?>
                                </div>
                                                           

                                <!--<div class="clearfix">
                                    <label>Pedido <em>*</em><small>Pededio a realizar</small></label>
                                    <!-- llamado del controlador y el metodo -->
                                    <?php //echo Pedido_controller::getListPedido("IdPedido"); ?>
                                <!--</div>-->

                                                                
                                <div class="action clearfix">
                                    <button class="button button-gray" type="submit"><span class="accept"></span>OK</button>
                                    <button class="button button-gray" type="reset">Reset</button>
                                </div>
                            </form>
                        </section>
                    </div>
                    <!-- End Forms Section -->

                    <!-- Accordion Section -->
                    <div class="main-content grid_3 omega">
                        <header><h2>Instrucciones</h2></header>
                        <section class="accordion clearfix">
                            <header class="current"><h2>Registro de Usuarios</h2></header>
                            <img src=""; ></img>
                        </section>
                    </div>
                    <!-- End Accordion Section -->

                    <div class="clear"></div>

                </section>

                <?php require_once("snippets/footer.php"); ?>
                <!-- Main Section End -->

            </div>
        </section>
    </div>

</body>
</html>
